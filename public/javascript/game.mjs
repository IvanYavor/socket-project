const username = sessionStorage.getItem("username");
const discBtn = document.getElementById("disconnect");
const userList = document.getElementById("users");
const readyBtn = document.getElementById("ready-btn");
const textField = document.getElementById("text");
const progressBars = document.getElementsByTagName("progress");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const updateUserList = (users) => {
  const ready = "circle circle-ready";
  const notReady = "circle circle-not-ready";
  userList.innerHTML = `
    ${users.users
      .map(
        (user) =>
          `<li>
          <span class="${user.ready ? ready : notReady}"></span>
          ${user.username} ${user.id === socket.id ? "(you)" : ""}</li>
          <progress  class="${
            user.progress >= 100 ? "progress-complete" : ""
          }" max="100" value="${user.progress}"></progress>`
      )
      .join("")}
  `;
};

const usernameExistsAlert = () => {
  alert("This username already exists");
  sessionStorage.clear();
  window.location.replace("/login");
};

const startTimer = () => {
  const body = document.getElementsByTagName("body")[0];
  const timer = document.createElement("div");
  timer.setAttribute("id", "timer");
  body.appendChild(timer);
};

const updateTimer = (time) => {
  const timer = document.getElementById("timer");
  timer.innerHTML = time;
};

const stopTimer = () => {
  const timer = document.getElementById("timer");
  timer.remove();
  outputText();

  document.addEventListener("keypress", typing, false);
};

const calcIncrease = (allChars) => {
  return (1 / allChars) * 100;
};

const typing = (e) => {
  let typed = String.fromCharCode(e.which);
  const spans = document.querySelectorAll(".character");
  for (let i = 0; i < spans.length; i++) {
    if (spans[i].innerHTML === typed) {
      if (spans[i].classList.contains("bg")) {
        // if it already has class with the bacground color then check the next one
        continue;
      } else if (
        (spans[i].classList.contains("bg") === false &&
          spans[i - 1] === undefined) ||
        spans[i - 1].classList.contains("bg") !== false
      ) {
        // if it dont have class, if it is not first letter or if the letter before it dont have class (this is done to avoid marking the letters who are not in order for being checked, for example if you have two "A"s so to avoid marking both of them if the first one is at the index 0 and second at index 5 for example)
        spans[i].classList.add("bg");
        socket.emit("USER_TYPED", calcIncrease(spans.length));

        break;
      }
    }
  }

  let checked = 0;
  for (let i = 0; i < spans.length; i++) {
    if (spans[i].className === "character bg") {
      checked++;
    }
    if (checked === spans.length) {
      document.removeEventListener("keypress", typing, false);
      socket.emit("USER_DONE");
      textField.innerHTML = "Success";
    }
  }
};

const outputText = () => {
  textField.style.display = "block";
};

const setText = (text) => {
  textField.style.display = "none";
  let textStr = "";
  for (const ch of text) {
    textStr += `<span class="character">${ch}</span>`;
  }
  textField.innerHTML = `${textStr}`;
};

const getTextById = (id) => {
  axios.get(`http://localhost:3002/game/texts/${id}`).then((data) => {
    const text = data.data.text;
    setText(text);
  });
};

const gameDone = (winners) => {
  alert(`${winners
    .map((winner, index) => `${index + 1}. ${winner.username}\n`)
    .join("")}
  `);
};

socket.on("USERNAME_ALREADY_EXISTS", usernameExistsAlert);
socket.on("UPDATE_USER_LIST", updateUserList);
socket.on("UPDATE_TIMER", updateTimer);
socket.on("START_TIMER", startTimer);
socket.on("STOP_TIMER", stopTimer);
socket.on("TEXT_ID", getTextById);
socket.on("GAME_DONE", gameDone);

discBtn.addEventListener("click", (e) => {
  socket.emit("disconnect");
  sessionStorage.clear();
  window.location.replace("/login");
});

readyBtn.addEventListener("click", (e) => {
  socket.emit("USER_READY");
  readyBtn.innerHTML = "Not Ready";
  readyBtn.disabled = true;
});

window.addEventListener("load", () => {
  let arrayProgress = Array.from(progressBars);
  arrayProgress.map((progress) => {
    progress.addEventListener("valueChanged", (e) => {
      console.log(e.target);
    });
  });
});
