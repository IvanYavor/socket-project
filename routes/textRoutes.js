import { Router } from "express";
import { texts } from "../data";

const router = Router();

router.get("/:id", (req, res) => {
  const id = req.params.id;
  const text = texts[id];
  res.status(200).json({ text });
});

export default router;
