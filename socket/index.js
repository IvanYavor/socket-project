import * as config from "./config";
import { SECONDS_TIMER_BEFORE_START_GAME } from "./config";
import {
  userJoin,
  usernameExist,
  userLeave,
  getUsers,
  isLogged,
  makeUserReady,
  isEveryUserReady,
  increaseProgress,
  addToWinnerList,
  isGameDone,
  getWinners,
} from "../utils/users";
import { texts } from "../data";

const startGame = (io) => {
  sendIdText(io);
  startTimer(io);
};

const startTimer = (io) => {
  let counter = SECONDS_TIMER_BEFORE_START_GAME;

  const timer = setInterval(() => {
    io.emit("UPDATE_TIMER", counter);
    counter--;
    if (counter === 0) {
      io.emit("STOP_TIMER");
      clearInterval(timer);
    }
  }, 1000);
};

const sendIdText = (io) => {
  const randomId = Math.floor(Math.random() * texts.length);
  io.emit("TEXT_ID", randomId);
};

export default (io) => {
  io.on("connection", (socket) => {
    const username = socket.handshake.query.username;

    if (usernameExist(username)) {
      io.emit("USERNAME_ALREADY_EXISTS");
      return;
    }

    if (username !== "null") {
      const user = userJoin(socket.id, username);
      if (user) console.log(`${user.username} joined`);
    }

    io.emit("UPDATE_USER_LIST", {
      users: getUsers(),
    });

    socket.on("USER_READY", () => {
      makeUserReady(socket.id);
      io.emit("UPDATE_USER_LIST", {
        users: getUsers(),
      });

      if (isEveryUserReady()) {
        io.emit("START_TIMER");
        startGame(io);
      }
    });

    socket.on("USER_TYPED", (increase) => {
      increaseProgress(socket.id, increase);
      io.emit("UPDATE_USER_LIST", {
        users: getUsers(),
      });
    });

    socket.on("USER_DONE", () => {
      addToWinnerList(socket.id);
      if (isGameDone()) {
        const winners = getWinners();
        io.emit("GAME_DONE", winners);
        return;
      }
    });

    socket.on("disconnect", () => {
      const user = userLeave(socket.id);
      if (user) {
        console.log(`${user.username} disconnected`);
        io.emit("UPDATE_USER_LIST", {
          users: getUsers(),
        });
      }
    });
  });
};
