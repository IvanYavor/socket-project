const users = [];
const winners = [];

const userJoin = (id, username) => {
  const user = users.find((user) => user.username === username);
  if (!user) {
    const user = { id, username, ready: false, progress: 0 };
    users.push(user);
    return user;
  }

  return user;
};

const increaseProgress = (id, value) => {
  const index = users.findIndex((user) => user.id === id);
  if (index !== -1) {
    users[index].progress += value;
  }
};

const makeUserReady = (id) => {
  const index = users.findIndex((user) => user.id === id);
  if (index !== -1) {
    users[index].ready = true;
  }
};

const usernameExist = (username) => {
  const index = users.findIndex((user) => user.username === username);
  if (index === -1) {
    return false;
  }

  return true;
};

const isLogged = (id) => {
  const index = users.findIndex((user) => user.id === id);
  if (index === -1) {
    return false;
  }
  return true;
};

const userLeave = (id) => {
  const index = users.findIndex((user) => user.id === id);

  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
};

const isEveryUserReady = () => {
  if (users.some((user) => user.ready === false)) return false;

  return true;
};

const getUsers = () => {
  return users;
};

const getUserById = (id) => {
  const index = users.findIndex((user) => user.id === id);
  if (index === -1) {
    return null;
  }

  return users[index];
};

const addToWinnerList = (id) => {
  winners.push(getUserById(id));
};

const isGameDone = () => {
  if (winners.length >= users.length) {
    return true;
  }

  return false;
};

const getWinners = () => {
  return winners;
};

module.exports = {
  userJoin,
  usernameExist,
  userLeave,
  getUsers,
  isLogged,
  makeUserReady,
  isEveryUserReady,
  increaseProgress,
  addToWinnerList,
  isGameDone,
  getWinners,
};
